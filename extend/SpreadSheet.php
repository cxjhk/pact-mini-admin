<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

class SpreadSheet
{
    public static function getSpreadSheet($xlsx){

        $spreadsheet = IOFactory::load($xlsx);
        // 获取活动表
        $worksheet = $spreadsheet->getActiveSheet();

        // 获取行数和列数
        $rows = $worksheet->getHighestRow();
        //$columns = $worksheet->getHighestColumn();
        $columns = 'I'; // 设置要读取的列范围

// 循环读取数据
        $arr = [];
        for ($row = 2; $row < $rows; $row++) {
            $rowData = []; // 用于存储当前行的数据
            for ($column = 'A'; $column <= $columns; $column++) {
                $cellValue = $worksheet->getCell($column . $row)->getValue();
                $rowData[] = $cellValue; // 无论是否为空都添加到 $rowData 中
            }
            $arr[] = $rowData; // 将当前行的数据添加到 $arr 中
        }
        dd($arr);
    }

    public static function test($xlsx){

        $excel = new \Vtiful\Kernel\Excel();
// 读取测试文件
        $data = $excel->openFile($xlsx)
            ->openSheet()
            ->getSheetData();

        var_dump($data); // [['Item', 'Cost']]
    }
}