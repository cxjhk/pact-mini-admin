<?php
// +----------------------------------------------------------------------
// | B5ThinkCMF [快捷通用基础开发管理平台]
// +----------------------------------------------------------------------
// | Author: 冰舞 <357145480@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\admin\controller\signing;

use app\admin\BaseController;
use app\admin\extend\helpers\TraitActionHelper;
use app\common\model\signing\SigningContract;


class SigningContractController extends BaseController
{
    use TraitActionHelper;
    protected $model = SigningContract::class;


}
