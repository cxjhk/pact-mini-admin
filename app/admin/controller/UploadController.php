<?php

namespace app\admin\controller;

use app\admin\BaseController;
use app\common\helpers\Result;
use Vtiful\Kernel\Excel;

class UploadController extends BaseController
{

    public function getCOSUploadUrlAction()
    {
        return Result::success('ok', [
            'url_params' => \Wechat::getUploadUrl(
                $this->request->post('path')
            ),
            'env' => [
                'appId' => env('TENCENT.APP_ID'),
                'env' => env('TENCENT.CLOUD_ENV')
            ]
        ]);
    }

    public function uploadAction()
    {

        $file = request()->file('file');

        // 文件上传目录
        $upload_dir = 'uploads/';

        $filename = \think\facade\Filesystem::disk('public')->putFile('docx', $file);

        // 将文件移动到上传目录
        if (!empty($filename)) {
            return Result::success('ok', [
                'url' => $upload_dir . $filename
            ]);
        } else {
            return Result::error('上传失败');
        }

    }

    public function uploadStudentAction()
    {
        $file = request()->file('file');

        $filename = \think\facade\Filesystem::disk('public')->putFile('student', $file);

        $excel = new Excel(['path' => 'uploads']);

        $data = $excel
            ->openFile($filename)
            ->openSheet("Sheet1")
            ->getSheetData();

        foreach ($data as $datum) {
            dump($datum);
        }
        die;
        return ['data' => $data];
    }
}