<?php
// +----------------------------------------------------------------------
// | B5ThinkCMF [快捷通用基础管理开发平台]
// +----------------------------------------------------------------------
// | Author: 冰舞 <357145480@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\common\model\contract;

use app\common\BaseModel;

class SigningContract extends BaseModel
{
    protected $table = 'signing_contract';


    public function insert($data, $getLastInsID = false)
    {
        $query = self::newQuery($this->table);
        $data['created_at'] = time();
        return $query->strict(false)->insert($data, $getLastInsID);
    }

    public function insertAll($data)
    {
        $data = array_map(function ($item) {
            $item['created_at'] = time();
            return $item;
        }, $data);
        $query = self::newQuery($this->table);
        return $query->replace()->insertAll($data);
    }
}
