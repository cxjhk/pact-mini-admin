<?php
// +----------------------------------------------------------------------
// | B5ThinkCMF [快捷通用基础管理开发平台]
// +----------------------------------------------------------------------
// | Author: 冰舞 <357145480@qq.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\common\model\contract;

use app\common\BaseModel;
use think\facade\Db;

class Contract extends BaseModel
{
    protected $table = 'contract';


    public function getStatusCount($status = null)
    {
        $query = self::newQuery($this->table);
        if ($status !== null) {
            $query->where('status', $status);
        }

        return $query->count('id');
    }


    public function insert($data, $getLastInsID = false)
    {
        $query = self::newQuery($this->table);
        $data['created_at'] = time();
        return $query->strict(false)->insert($data, $getLastInsID);
    }

    public function update($where, $data)
    {
        $query = self::newQuery($this->table);
        return $query->where($where)->update($data);
    }

}
