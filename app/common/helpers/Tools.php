<?php

namespace app\common\helpers;

use think\facade\Session;

class Tools
{
    public static function dd($data,$isDump=true,$isDie=true){
        echo "<pre>";
        if($isDump){
            var_dump($data);
        }else{
            print_r($data);
        }
        echo "</pre>";
        if ($isDie){
            die;
        }
    }

    public static function getOperator(){
        $info = Session::get('adminLoginInfo');
        return $info['info']['id'];
    }

    public static function getNo(): string
    {
        return uniqid('SN.');
    }
}